﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CacheEngine
{
    /// <summary>过期时间接口</summary>
    public interface IExpirationTime
    {
        /// <summary>取得过期时间</summary>
        /// <returns></returns>
        DateTime GetExpirationDate();
    }
}
