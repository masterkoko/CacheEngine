﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CacheEngine
{
    /// <summary>缓存管理接口</summary>
    public interface ICacheManager
    {
        /// <summary>尝试取得缓存内容</summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="methodInfo"></param>
        /// <param name="params"></param>
        /// <param name="cachedItem"></param>
        /// <returns></returns>
        bool TryGetCacheItem<T>(MethodInfo methodInfo, object[] @params, out T cachedItem);
        
        /// <summary>更新缓存内容</summary>
        /// <param name="objectType"></param>
        /// <param name="methodInfo"></param>
        /// <param name="params"></param>
        /// <param name="item"></param>
        void SaveOrUpdate(Type objectType, MethodInfo methodInfo, object[] @params, object item);

        /// <summary>缓存实现引擎</summary>
        ICacheEngine CacheEngine { get; set; }

        /// <summary>关键字生成器引擎</summary>
        IKeyGenerator KeyGenerator { get; set; }

        /// <summary>缓存引擎自定义属性</summary>
        CacheEngineAttribute Attribute { get; set; }
    }
}
