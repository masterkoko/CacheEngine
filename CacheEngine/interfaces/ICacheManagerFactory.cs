﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Text;

namespace CacheEngine
{
    /// <summary>缓存管理工厂</summary>
    public interface ICacheManagerFactory
    {
        /// <summary>尝试获取缓存管理器</summary>
        /// <param name="methodInfo"></param>
        /// <param name="cacheManager"></param>
        /// <returns></returns>
        bool TryGetCacheManager(MethodInfo methodInfo, out ICacheManager cacheManager);  
    }
}
